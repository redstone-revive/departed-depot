# Departed Depot

*Developed by the Redstone Revive Team*

Departed Depot is a graves plugin for Minecraft Spigot servers, tested on a local Paper 1.20.4 server with Java 17.
Players will spawn graves when they die to one of the configured causes.
They appear as a player head on the block they died at.
These graves can only be claimed by the player who died for a set duration before it can be claimed by anyone.

## Features

- Create a depot whenever a player dies!
- Players can reclaim their items and xp by right-clicking their player head.
- Configurable protection duration. 
- Include or exclude specific death causes.

## Build Guide

1) Clone the repository.
    ```
   git clone https://gitlab.com/redstone-revive/departed-depot
   cd departed-depot
   ```
2) Build using Gradle.
   Windows
    ```shell
   gradlew.bat
   ```
    Linux
     ```shell
    ./gradlew build
    ```
   Your completed jar will be in `/build/libs/`.

## Installation

1) Obtain a copy of the jar.
    This can be done by following the build guide or by downloading the package from the releases page.
2) Copy into `./plugins`
    Then just start your server!

## API For Developers

We are currently experiencing an issue publishing a remote maven package for this repository.
You can clone the repository and run `gradle publishToMavenLocal` to obtain the package.
Then add the dependency.

Gradle
```groovy
dependencies {
    compileOnly 'com.gitlab.redstonerevive:departed-depot:1.0.0-ALPHA'
}
```

Maven
```xml
<dependencies>
    <dependency>
        <groupId>com.gitlab.redstonerevive</groupId>
        <artifactId>departed-depot</artifactId>
        <version>1.0.0-ALPHA</version>
        <scope>provided</scope>
    </dependency>
</dependencies>
```

## Support

*For any additional questions please visit our [discord](https://discord.gg/EfprEUTnXQ)!*

## Contributors

- Sugaku
