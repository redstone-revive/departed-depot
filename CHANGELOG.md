# 1.3.1-ALPHA

## Bug Fixes
- Fixed a bug where Depots could be destroyed by tnt and creeper explosions.
- Fixed a bug where flowing fluids would break depots.
- Fixed a bug causing Depots to not spawn when WorldGuard was missing.

# 1.3.0-ALPHA

## Features
- Added worldguard flag to deny depot spawning.
- Added WorldGuard hook configuration options.

## Bug Fixes
- Fixed an error that occurs when WorldGuard is not present.
- Fixed a bug preventing DepartedDepot from launching.
- Minor optimizations and refactoring.

## API Changes
- Marked `Depot#getContents()` and `Depot#getXp()` as deprecated.

# 1.2.0-ALPHA

## Features
- Added consideration for unknown death reasons.
- Implemented depot despawning.
- Added despawn related config options.
- Added DepotManager#addDepot(Depot) method.

## Bug Fixes
- Fixed DepotManager's update task not properly shutting down.
- Fixed a bug where players could keep accessing inventory even if the Depot was despawned.
- Fixed DepotManager's update task not properly shutting down.

# 1.1.0-ALPHA

## Features
- Re-bound claiming to shift + right-click.
- Depots can now be opened as chests with right-click.

## Bug Fixes
- Fixed a bug where depot contents were not updating properly. - Covered an oversight were players could lose their exp.
- Fixed and issue preventing chests from being opened on most content lengths.


# 1.0.1-ALPHA

## Bug Fixes
- Fixed a bug where players wouldn't get items if their inventory was full.
- Fixed a bug where finding a depot would fail if in a different world.
- Fixed a bug where protection method counted up instead of down.


# 1.0.0-ALPHA

- Created the minimum necessary features to be considered a 'graves' plugin.
