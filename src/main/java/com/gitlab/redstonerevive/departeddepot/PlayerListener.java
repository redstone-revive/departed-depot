package com.gitlab.redstonerevive.departeddepot;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

/**
 * The PlayerListener is dedicated specifically to player events. It attempts to resolve every event as quickly as
 * possible to prevent any hangs on the main thread.
 *
 * @author Sugaku
 */
public class PlayerListener implements Listener {

    /**
     * We grab the instance of the DepotManager since we'll likely use it for most methods.
     */
    private static final DepotManager depotManager = DepotManager.getInstance();

    /**
     * Called whenever a player interacts with a block. We need this to check if a player has right-clicked a Depot head.
     *
     * @param event The player interact event.
     */
    @EventHandler
    public void onInteract (PlayerInteractEvent event) {
        if (!event.hasBlock()) return;
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
        Block clickedBlock = event.getClickedBlock();
        if (clickedBlock == null) return;
        if (clickedBlock.getType() != Material.PLAYER_HEAD) return;
        Depot depot = depotManager.getDepot(clickedBlock.getLocation());
        if (depot == null) return;
        event.setCancelled(true);
        Player player = event.getPlayer();
        long millis = System.currentTimeMillis() - depot.millisCreationTime();
        if (millis < ConfigManager.getInstance().getDepotProtection() * 1000L) {
            if (!depot.isOwner(player)) {
                player.sendMessage(ChatColor.RED + "You cannot access this for another " + (ConfigManager.getInstance().getDepotProtection() - (millis / 1000)) + "s.");
                return;
            }
        }
        if (player.isSneaking()) {
            if (depot.giveContents(player))
                depotManager.removeDepot(depot);
            else
                player.sendMessage(ChatColor.RED + "Someone is currently viewing this.");
        } else
            if (!depot.openAsChest(player, depotManager.indexOf(depot)))
                player.sendMessage(ChatColor.RED + "Someone is already viewing this.");
    }

    /**
     * Called whenever a player dies. When this happens we need to remove the item and xp drops as well as spawn a Depot.
     *
     * @param event The player death event.
     */
    @EventHandler
    public void onPlayerDeath (PlayerDeathEvent event) {
        EntityDamageEvent lastDamaged = event.getEntity().getLastDamageCause();
        EntityDamageEvent.DamageCause cause;
        if (lastDamaged == null) cause = null;
        else cause = lastDamaged.getCause();
        ConfigManager configManager = ConfigManager.getInstance();
        // configManager.isDeathReasonsWhitelist() ? configManager.getDeathReasons().contains(cause) : !configManager.getDeathReasons().contains(cause)
        // The following is logically equivalent to above.
        if (configManager.isDeathReasonsWhitelist() == configManager.getDeathReasons().contains(cause)) {
            event.setDroppedExp(0);
            event.getDrops().clear();
            depotManager.addDepot(event.getEntity());
        }
    }

    /**
     * Called whenever a player breaks a block. We attempt to clear out as many calls as early as possible to prevent
     * hangs.
     *
     * @param event The block break event.
     */
    @EventHandler
    public void onBreak (BlockBreakEvent event) {
        if (event.getBlock().getType() != Material.PLAYER_HEAD) return;
        Depot depot = depotManager.getDepot(event.getBlock().getLocation());
        if (depot == null) return;
        event.setCancelled(true);
    }

    /**
     * Called whenever an Inventory is closed. We need this to check if a Depot is closed.
     *
     * @param event The Inventory closed event.
     */
    @EventHandler
    public void onInventoryClose (InventoryCloseEvent event) {
        String title = event.getView().getTitle();
        if (title.contains("Depot: ")) {
            ItemStack[] contents = event.getView().getTopInventory().getStorageContents();
            Integer index = Integer.parseInt(title.split("Depot: ")[1]);
            Depot depot = depotManager.getDepot(index);
            ArrayList<ItemStack> toUpdate = new ArrayList<>();
            for (ItemStack i : contents) if (i != null) toUpdate.add(i);
            contents = new ItemStack[toUpdate.size()];
            toUpdate.toArray(contents);
            depot.updateItems(contents);
            depot.closedChest();
            if (contents.length == 0) {
                depot.giveContents((Player) event.getPlayer());
                depotManager.removeDepot(depot);
            }
        }
    }
}
