package com.gitlab.redstonerevive.departeddepot;

import com.gitlab.redstonerevive.departeddepot.hooks.HookManager;
import com.gitlab.redstonerevive.departeddepot.hooks.WorldGuardHook;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Creates, saves, loads, tracks, and removes Depots during runtime. The Depot manager follows the singleton design
 * pattern.
 *
 * @author Sugaku
 */
public class DepotManager {

    /**
     * The active DepotManager instance.
     */
    private static DepotManager instance = null;

    /**
     * A map of Depots the DepotManager is keeping track of.
     */
    private final Map<Integer, Depot> depots = new HashMap<>();

    /**
     * An ID generator to prevent collisions.
     */
    private AtomicInteger idGen = new AtomicInteger(0);

    /**
     * Reference to the update task, so it can be canceled on disable.
     */
    private BukkitRunnable updateTask;

    /**
     * Creates a new DepotManager. Note: This will attempt to load existing depots from the disk.
     */
    private DepotManager () {
        Logger logger = DepartedDepot.getInstance().getLogger();
        File dataFolder = DepartedDepot.getInstance().getDataFolder();
        File saveFile = new File(dataFolder.getPath() + "/depots.yml");
        if (!dataFolder.exists()) dataFolder.mkdir();
        if (!saveFile.exists()) {
            logger.log(Level.WARNING, "We could not locate an existing depot save file. This is only a concern if this is NOT your first time running Departed Depots.");
            return;
        }
        YamlConfiguration save = new YamlConfiguration();
        try {
            save.load(saveFile);
            loadDepots(save);
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "We could not load your save file: " + exception.getMessage());
            logger.log(Level.SEVERE, "This can be due to filesystem permission issues or a bad path on our part.");
            logger.log(Level.SEVERE, "You can seek support in our discord: https://discord.com/invite/EfprEUTnXQ");
            logger.log(Level.SEVERE, "We will likely ask for your console logs and specific filesystem information we can help you obtain.");
        } catch (InvalidConfigurationException exception) {
            logger.log(Level.SEVERE, "Your save file is corrupted: " + exception.getMessage());
            logger.log(Level.SEVERE, "Try deleting it and restarting to generate a new one, or seek support in our discord: https://discord.com/invite/EfprEUTnXQ");
            logger.log(Level.SEVERE, "We will likely ask for the save file contents and your console logs.");
        }
        updateTask = new BukkitRunnable() {
            @Override
            public void run() {
                update();
            }
        };
        updateTask.runTaskTimer(DepartedDepot.getInstance(), 0, 20);
    }

    /**
     * Accessor method for the active manager.
     *
     * @return The active DepotManager instance.
     */
    public static DepotManager getInstance () {
        if (instance == null) instance = new DepotManager();
        return instance;
    }

    /**
     * Loads all Depots contained within the given YamlConfiguration into the depots map.
     *
     * @param save The save data to load into the depots map. Assumed to be loaded from a valid file with good data.
     */
    private void loadDepots (YamlConfiguration save) {
        Logger logger = DepartedDepot.getInstance().getLogger();
        for (String s : save.getKeys(false)) {
            try {
                int index = Integer.parseInt(s);
                if (idGen.get() < index) idGen = new AtomicInteger(index + 1);
                depots.put(index, new Depot(save.getConfigurationSection(s)));
            } catch (NumberFormatException exception) {
                logger.log(Level.SEVERE, "We could not load the depot at index: " + s);
                logger.log(Level.SEVERE, "This will not effect other depots.");
                logger.log(Level.SEVERE, "You can seek support in our discord: https://discord.com/invite/EfprEUTnXQ");
                logger.log(Level.SEVERE, "We will likely as for the save file contents.");
            }
        }
    }

    /**
     * Saves all Depots to the hard drive. Will overwrite any existing data save files.
     */
    public void save () {
        Logger logger = DepartedDepot.getInstance().getLogger();
        File dataFolder = DepartedDepot.getInstance().getDataFolder();
        File saveFile = new File(dataFolder.getPath() + "/depots.yml");
        if (!dataFolder.exists()) dataFolder.mkdir();
        YamlConfiguration save = new YamlConfiguration();
        depots.forEach((i, d) -> d.save(save.createSection(i + "")));
        try {
            save.save(saveFile);
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "We are unable to save depot data: " + exception.getMessage());
            logger.log(Level.SEVERE, "You can seek support in our discord: https://discord.com/invite/EfprEUTnXQ");
            logger.log(Level.SEVERE, "We will likely ask for your console logs.");
        }
    }

    /**
     * Obtains, if preset, the depot at the given location.
     *
     * @param locale The location to check for depots at.
     * @return The governing Depot or null.
     */
    public Depot getDepot (Location locale) {
        for (Depot d : depots.values()) {
            if (!locale.getWorld().getName().equalsIgnoreCase(d.getLocale().getWorld().getName())) continue;
            if (locale.distance(d.getLocale()) < 0.5)
                return d;
        }
        return null;
    }

    /**
     * Obtains, if present, the Depot with the given ID.
     *
     * @param id The ID of the depot to get.
     * @return The Depot, or null.
     */
    public Depot getDepot (Integer id) {
        return depots.get(id);
    }

    /**
     * Adds and creates a new Depot to the DepotManager based on the given Player state.
     *
     * @param player The player to add to the Depot.
     */
    public void addDepot (Player player) {
        Depot depot = new Depot(player);
        addDepot(depot);
    }

    /**
     * Adds and creates a new Depot to the DepotManager based on the given Depot state.
     *
     * @param depot To register into the system.
     */
    public void addDepot (Depot depot) {
        WorldGuardHook worldGuard = HookManager.getInstance().getWorldGuardHook();
        if (worldGuard != null && worldGuard.allowSpawn(depot.getLocale())) {
            if (ConfigManager.getInstance().getOnWorldGuardDeny().equalsIgnoreCase("DROP"))
                depot.dropAll(depot.getLocale());
        } else {
            depot.spawnDepot();
            depots.put(idGen.getAndIncrement(), depot);
        }
    }

    /**
     * Removes the given Depot from the list of active Depots.
     *
     * @param depot The Depot to remove from the Depot list.
     */
    public void removeDepot (Depot depot) {
        depots.values().remove(depot);
        depot.despawnDepot();
    }

    /**
     * A method to locate the ID of a Depot from the Depot.
     *
     * @param depot The Depot to determine the ID of.
     * @return The index of the Depot in the DepotManager. Otherwise, null.
     */
    public int indexOf (Depot depot) {
        for (Integer i : depots.keySet())
            if (depot.equals(depots.get(i)))
                return i;
        return -1;
    }

    /**
     * The update task checks all depots to see if they are over the despawn timer.
     */
    public void update () {
        if (ConfigManager.getInstance().isDespawnEnabled()) {
            int timer = ConfigManager.getInstance().getDespawnTimer();
            List<Integer> toRemove = new ArrayList<>();
            for (Depot d : depots.values())
                if (System.currentTimeMillis() - d.millisCreationTime() >= timer * 1000L)
                    toRemove.add(indexOf(d));
            for (Integer i : toRemove) {
                Depot d = getDepot(i);
                if (d == null) continue;
                if (d.isOpened()) continue;
                if (ConfigManager.getInstance().getDoOnDespawn().equalsIgnoreCase("DROP"))
                    d.dropAll(d.getLocale());
                removeDepot(d);
            }
        }
    }

    /**
     * An accessor method to the update task so that it can be canceled on disable.
     *
     * @return Reference to the update task, so it can be properly shutdown.
     */
    public BukkitRunnable getUpdateTask () {
        return updateTask;
    }
}
