package com.gitlab.redstonerevive.departeddepot;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.entity.EntityDamageEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The ConfigManager manages config options set in the config file as well as updating the config file.
 *
 * @author Sugaku
 */
public class ConfigManager {

    /**
     * The active ConfigManager instance.
     */
    private static ConfigManager instance = null;

    /**
     * Whether the list of actionable death reasons is in whitelist mode.
     */
    private boolean deathReasonsWhitelist = true;

    /**
     * The list of actionable death reasons.
     */
    private List<EntityDamageEvent.DamageCause> deathReasons = new ArrayList<>(List.of(EntityDamageEvent.DamageCause.values()));

    /**
     * The amount of time, in seconds, that Depot protection lasts.
     */
    private int depotProtection = 300;

    /**
     * Whether depots should be despawned after the given duration.
     */
    private boolean doDespawn = true;

    /**
     * The amount of time, in seconds, that Depots should be despawned after.
     */
    private int depotDespawnTimer = 600;

    /**
     * What to do once the Depot has been despawned.
     */
    private String doOnDespawn = "DROP";

    /**
     * Whether the WorldGuard hook should be utilized or not.
     */
    private boolean useWorldGuard = true;

    /**
     * The action to take when WorldGuard denies depot spawning.
     */
    private String onWorldGuardDeny = "DROP";

    /**
     * Creates a new ConfigManager by loading the options set in the local config.
     */
    private ConfigManager () {
        Logger logger = DepartedDepot.getInstance().getLogger();
        File dataFolder = DepartedDepot.getInstance().getDataFolder();
        File configFile = new File(dataFolder.getPath() + "/config.yml");
        if (!dataFolder.exists()) dataFolder.mkdir();
        if (!configFile.exists()) DepartedDepot.getInstance().saveDefaultConfig();

        YamlConfiguration config = new YamlConfiguration();
        try {
            config.load(configFile);
            loadOptions(config);
        } catch (IOException exception) {
            logger.log(Level.SEVERE, "We could not load your config file: " + exception.getMessage());
            logger.log(Level.SEVERE, "This can be due to filesystem permission issues or a bad path on our part.");
            logger.log(Level.SEVERE, "You can seek support in our discord: https://discord.com/invite/EfprEUTnXQ");
            logger.log(Level.SEVERE, "We will likely ask for your console logs and specific filesystem information we can help you obtain.");
        } catch (InvalidConfigurationException exception) {
            logger.log(Level.SEVERE, "Your config is corrupted: " + exception.getMessage());
            logger.log(Level.SEVERE, "Try deleting it and restarting to generate a new one, or seek support in our discord: https://discord.com/invite/EfprEUTnXQ");
            logger.log(Level.SEVERE, "We will likely ask for the config file contents and your console logs.");
        }
    }

    /**
     * Accessor method to the active ConfigManager instance.
     *
     * @return The active ConfigManager instance.
     */
    public static ConfigManager getInstance () {
        if (instance == null) instance = new ConfigManager();
        return instance;
    }

    /**
     * Loads all the options in the config to their corresponding fields.
     *
     * @param config The loaded config file. Assumed to contain good data.
     */
    private void loadOptions (YamlConfiguration config) {
        depotProtection = config.getInt("claiming.owner-protection");
        deathReasonsWhitelist = "WHITELIST".equalsIgnoreCase(config.getString("spawning.death-reasons.mode"));
        for (String s : config.getStringList("spawning.death-reasons.entries")) {
            if (s.equalsIgnoreCase("ALL")) {
                deathReasons = new ArrayList<>(List.of(EntityDamageEvent.DamageCause.values()));
                deathReasons.add(null);
                break;
            } else if (s.equalsIgnoreCase("NULL")) deathReasons.add(null);
            deathReasons.add(EntityDamageEvent.DamageCause.valueOf(s.toUpperCase()));
        }
        doDespawn = config.getBoolean("despawn.enabled", true);
        depotDespawnTimer = config.getInt("despawn.time", 600);
        doOnDespawn = config.getString("despawn.on-despawn", "DROP");
        useWorldGuard = config.getBoolean("world-guard.enabled", true);
        onWorldGuardDeny = config.getString("world-guard.on-deny", "DROP");
    }

    /**
     * Accessor method for the number of seconds Depot protection lasts for.
     *
     * @return The number of seconds Depot protection lasts.
     */
    public int getDepotProtection () {
        return depotProtection;
    }

    /**
     * Accessor method for whether the death reasons list is supposed to be considered as a whitelist.
     *
     * @return Whether the death reasons list is a whitelist.
     */
    public boolean isDeathReasonsWhitelist () {
         return deathReasonsWhitelist;
    }

    /**
     * Accessor method for the list of death reasons.
     *
     * @return The list of entries in the death reasons list.
     */
    public List<EntityDamageEvent.DamageCause> getDeathReasons () {
        return deathReasons;
    }

    /**
     * An accessor method for whether despawning is enabled.
     *
     * @return Whether despawning is enabled.
     */
    public boolean isDespawnEnabled () {
        return doDespawn;
    }

    /**
     * An accessor method for the amount of time, in seconds, before depots should be despawned.
     *
     * @return The despawn time value.
     */
    public int getDespawnTimer () {
        return depotDespawnTimer;
    }

    /**
     * An accessor method for what to do when despawning a depot.
     *
     * @return The process to do when despawning a depot.
     */
    public String getDoOnDespawn () {
        return doOnDespawn;
    }

    /**
     * Whether WorldGuard should be hooked into or not on load.
     *
     * @return True if WorldGuard hook is enabled.
     */
    public boolean useWorldGuard () {
        return useWorldGuard;
    }

    /**
     * What to do when WorldGuard denies spawning a depot.
     *
     * @return The action to take when depot spawning is disabled.
     */
    public String getOnWorldGuardDeny () {
        return onWorldGuardDeny;
    }
}
