package com.gitlab.redstonerevive.departeddepot.hooks;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.registry.FlagRegistry;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import com.sk89q.worldguard.protection.regions.RegionQuery;
import org.bukkit.Location;
import org.bukkit.World;

/**
 * The WorldGuardHook is used to create a new WorldGuard flag to allow or deny depot spawning in a protected region.
 *
 * @author Sugaku
 */
public class WorldGuardHook {

    /**
     * The allow depot flag instance.
     */
    private final StateFlag ALLOW_DEPOT_FLAG;

    /**
     * Creates the WorldGuardHook and attempts to create the new WorldGuard flag.
     */
    public WorldGuardHook () {
        FlagRegistry registry = WorldGuard.getInstance().getFlagRegistry();
        StateFlag flag = new StateFlag("depot-spawning", true);
        registry.register(flag);
        ALLOW_DEPOT_FLAG = flag;
    }

    /**
     * Checks whether this spawn is allowed in relevant WorldGuard regions.
     *
     * @param location The location to test.
     * @return True if depot spawning is allowed, otherwise false.
     */
    public boolean allowSpawn (Location location) {
        World w = location.getWorld();
        if (w == null) return true;
        RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        RegionQuery query = container.createQuery();
        ApplicableRegionSet set = query.getApplicableRegions(BukkitAdapter.adapt(location));
        return set.testState(null, ALLOW_DEPOT_FLAG);
    }
}
