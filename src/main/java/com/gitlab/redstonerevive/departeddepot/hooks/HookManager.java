package com.gitlab.redstonerevive.departeddepot.hooks;

import com.gitlab.redstonerevive.departeddepot.ConfigManager;

/**
 * The HookManager is used to offload hook loading in such a way that errors cannot cascade and snowball into the rest
 * of the plugin whilst also letting them be accessible.
 *
 * @author Sugaku
 */
public class HookManager {

    /**
     * The active HookManager instance.
     */
    private static HookManager instance;

    /**
     * The WorldGuardHook. Keep in mind that as a hook, this may be null.
     */
    private WorldGuardHook worldGuard;

    /**
     * Creates a new HookManager. The HookManager is created during onLoad().
     */
    private HookManager () {
        try {
            if (ConfigManager.getInstance().useWorldGuard()) worldGuard = new WorldGuardHook();
            else worldGuard = null;
        } catch (NoClassDefFoundError exception) {
            worldGuard = null;
        }
    }

    /**
     * Static accessor method for the HookManager.
     *
     * @return The active hook manager.
     */
    public static HookManager getInstance () {
        if (instance == null) instance = new HookManager();
        return instance;
    }

    /**
     * An accessor method for the WorldGuard hook. THIS MAY BE NULL.
     *
     * @return The WorldGuard hook.
     */
    public WorldGuardHook getWorldGuardHook () {
        return worldGuard;
    }
}
