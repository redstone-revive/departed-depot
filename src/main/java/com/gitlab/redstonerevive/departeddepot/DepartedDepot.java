package com.gitlab.redstonerevive.departeddepot;

import com.gitlab.redstonerevive.departeddepot.hooks.HookManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The Departed Depot plugin that adds graves to Minecraft servers. Players will spawn a grave when they die to a
 * configured cause.
 *
 * @author Sugaku
 */
public final class DepartedDepot extends JavaPlugin {

    /**
     * The active instance of the DepartedDepot plugin.
     */
    private static DepartedDepot instance;

    /**
     * The running version of DepartedDepot. This is updated by Gradle build as part of the processResources task.
     */
    private static final String VERSION = ""; // todo: This will likely need to be pulled directly from plugin.yml

    /**
     * Accessor method for the active plugin.
     *
     * @return The active DepartedDepot instance.
     */
    public static DepartedDepot getInstance () {
        assert instance != null; // Something went wrong if this fails. Should probably add some logging.
        return instance;
    }

    /**
     * A utility method to print environment information to the console.
     */
    private void environmentDetails () {
        getLogger().info("Running on: " + getServer().getVersion());
        getLogger().info("Plugin running: " + VERSION);
    }

    /**
     * Called when the plugin begins loading.
     */
    @Override
    public void onLoad () {
        instance = this;
        ConfigManager.getInstance();
        HookManager.getInstance();
    }

    /**
     * Called when this plugin is enabled.
     */
    @Override
    public void onEnable () {
        environmentDetails();
        DepotManager.getInstance();
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
    }

    /**
     * Called when this plugin is disabled.
     */
    @Override
    public void onDisable () {
        DepotManager.getInstance().save();
        DepotManager.getInstance().getUpdateTask().cancel();
    }
}
