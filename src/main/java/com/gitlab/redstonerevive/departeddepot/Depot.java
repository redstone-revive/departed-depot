package com.gitlab.redstonerevive.departeddepot;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Skull;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A Depot is a grave that has spawned as a result of a player's death.
 *
 * @author Sugaku
 */
public class Depot {

    /**
     * The creation time of this Depot. Used to determine if protection has passed.
     */
    private final long creationTime;

    /**
     * The location of this Depot. Will replace with a player head when spawned.
     */
    private Location locale;

    /**
     * The UUID of the owning player of this Depot.
     */
    private final UUID uuid;

    /**
     * The collection of items that are part of this Depot.
     */
    private ItemStack[] items;

    /**
     * The amount of xp that is in this Depot.
     */
    private final float xp;

    /**
     * This is the material that was replaced to place this Depot into the world.
     */
    private Material replaced = Material.AIR;

    /**
     * Whether this Depot is opened or not.
     */
    private boolean isOpened = false;

    /**
     * Creates a new Depot with the given parameters. This simply creates the object and does nothing to spawn the Depot.
     *
     * @param player The player that died to create this depot. Assumed to still be a valid object containing good data.
     */
    public Depot (Player player) {
        this(player.getLocation(),
                player.getUniqueId(),
                player.getLevel(),
                player.getInventory().getContents());
    }

    /**
     * Creates a new Depot from the data contained in the given ConfigurationSection.
     *
     * @param section The configuration section to pull data for this Depot from.
     */
    public Depot (ConfigurationSection section) {
        locale = section.getLocation("locale");
        uuid = UUID.fromString(section.getString("uuid", ""));
        xp = section.getInt("xp");
        List<ItemStack> swap = new ArrayList<>();
        ConfigurationSection inventory = section.getConfigurationSection("items");
        if (inventory != null)
            for (String s : inventory.getKeys(false))
                swap.add(section.getItemStack("items." + s));
        items = new ItemStack[swap.size()];
        swap.toArray(items);
        creationTime = System.currentTimeMillis();
    }

    /**
     * Creates a new Depot with the given parameters. This simply creates the object and does nothing to spawn the Depot.
     *
     * @param locale   The location of this Depot.
     * @param uuid     The UUID of the owning player.
     * @param xpLevels The amount of xp stored inside this Depot.
     * @param items    The items that will be contained within the Depot.
     */
    public Depot (Location locale, UUID uuid, int xpLevels, ItemStack[] items) {
        this.locale = locale;
        this.uuid = uuid;
        List<ItemStack> filter = new ArrayList<>();
        if (items != null)
            for (ItemStack i : items) {
                if (i == null) continue;
                if (i.hasItemMeta()) {
                    ItemMeta meta = i.getItemMeta();
                    if (meta != null) if (meta.hasEnchant(Enchantment.VANISHING_CURSE)) continue;
                }
                filter.add(i);
            }
        this.items = new ItemStack[filter.size()];
        filter.toArray(this.items);
        xp = Math.min(xpLevels * 7, 100); // This is the official formula for calculating xp: https://minecraft.wiki/w/Experience
        creationTime = System.currentTimeMillis();
    }

    /**
     * Checks whether the given player is the owner of this Depot or not.
     *
     * @param player The player to check for ownership.
     * @return True if, and only if, the given player owns this Depot.
     */
    public boolean isOwner (Player player) {
        return isOwner(player.getUniqueId());
    }

    /**
     * Checks whether the given UUID belongs to the owner of this Depot or not.
     *
     * @param uuid The UUID check.
     * @return True if, and only if, the given UUID belongs to the owning player.
     */
    public boolean isOwner (UUID uuid) {
        return uuid.equals(this.uuid);
    }

    /**
     * Accessor method for the time that this Depot was generated in millis.
     *
     * @return The time in millis of Depot creation.
     */
    public long millisCreationTime () {
        return creationTime;
    }

    /**
     * Spawns this Depot into the world using the contained values.
     */
    public void spawnDepot () {
        Block block = locale.getBlock();
        locale = block.getLocation();
        replaced = block.getType();
        block.setType(Material.PLAYER_HEAD);
        BlockState state = block.getState();
        if (state instanceof Skull skull)
            skull.setOwningPlayer(Bukkit.getOfflinePlayer(uuid));
        state.update();
    }

    /**
     * Removes this Depot from the world without dropping any items.
     */
    public void despawnDepot () {
        Block block = locale.getBlock();
        block.setType(replaced);
    }

    /**
     * Gives the contents of this Depot to the provided player. Will drop any remaining items onto the ground at their
     * feet.
     *
     * @param player The player to give the Depot contents to.
     * @return Whether items were successfully given or not.
     */
    public boolean giveContents (Player player) {
        if (isOpened) return false;
        HashMap<Integer, ItemStack> remainder = player.getInventory().addItem(items);
        if (!remainder.isEmpty())
            remainder.forEach((n, i) -> player.getWorld().dropItem(player.getLocation(), i));
        player.giveExp((int) xp);
        return true;
    }

    /**
     * Accessor method for the location of this Depot.
     *
     * @return The location of this Depot.
     */
    public Location getLocale () {
        return locale;
    }

    /**
     * Saves this Depot by mutating the given configuration section.
     *
     * @param section The configuration section to mutate to save this Depot.
     */
    public void save (ConfigurationSection section) {
        section.set("locale", locale);
        section.set("uuid", uuid.toString());
        section.set("xp", xp);
        AtomicInteger i = new AtomicInteger(0);
        for (ItemStack item : items)
            section.set("items." + i.getAndIncrement(), item);
    }

    /**
     * Opens the contents of this Depot like a chest to the given player, if it's not open.
     *
     * @param player The player to open this Depot to.
     * @param id     The ID of this Depot in the DepotManager.
     */
    public boolean openAsChest (Player player, Integer id) {
        if (isOpened) return false;
        Inventory inv = Bukkit.createInventory(player, ((items.length / 9) * 9) + 9, ChatColor.DARK_GRAY + "Depot: " + id);
        inv.setContents(items);
        player.openInventory(inv);
        isOpened = true;
        return true;
    }

    /**
     * Updates the local isOpened variable to the closed state.
     */
    public void closedChest () {
        isOpened = false;
    }

    /**
     * Updates the contents of the Depot's inventory to the given array.
     *
     * @param newItems The items  that now belong in the Depot's inventory.
     */
    public void updateItems (ItemStack[] newItems) {
        items = newItems;
    }

    /**
     * An accessor method to the contents of this depot.
     *
     * @return The contents of this depot.
     */
    @Deprecated(since = "1.3.0-ALPHA", forRemoval = true)
    public ItemStack[] getContents () {
        return items;
    }

    /**
     * An accessor method for the amount of xp stored in this depot.
     *
     * @return The xp amount stored in the depot.
     */
    @Deprecated(since = "1.3.0-ALPHA", forRemoval = true)
    public float getXp () {
        return xp;
    }

    /**
     * An accessor method to check whether this depot is open or not.
     *
     * @return True if this depot is currently being displayed to a player.
     */
    public boolean isOpened () {
        return isOpened;
    }

    /**
     * Drops the contents of this Depot at the given location.
     *
     * @param location The location to drop all this Depot's contents.
     */
    public void dropAll (Location location) {
        World w = location.getWorld();
        if (w == null) return;
        if (items == null) return;
        for (ItemStack i : items)
            w.dropItem(location, i);
        if (xp != 0) {
            ExperienceOrb orb = (ExperienceOrb) w.spawnEntity(location, EntityType.EXPERIENCE_ORB);
            orb.setExperience((int) xp);
        }
    }
}
