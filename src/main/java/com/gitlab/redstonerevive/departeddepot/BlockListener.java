package com.gitlab.redstonerevive.departeddepot;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

import java.util.List;

/**
 * The BlockListener is dedicated to block related events and is used to protect Depots.
 *
 * @author Sugaku
 */
public class BlockListener implements Listener {

    /**
     * We grab the instance of the DepotManager since we'll likely use it for most methods.
     */
    private static final DepotManager depotManager = DepotManager.getInstance();

    /**
     * Called whenever fluid attempts to flow into a new block or dragon egg attempts teleporting into a new block.
     *
     * @param event The block from-to event.
     */
    @EventHandler
    public void onBlockFromTo (BlockFromToEvent event) {
        if (event.getToBlock().getType() != Material.PLAYER_HEAD) return;
        Depot depot = depotManager.getDepot(event.getToBlock().getLocation());
        if (depot == null) return;
        event.setCancelled(true);
    }

    /**
     * Called whenever a block is blown up.
     *
     * @param event The block explode event to consider.
     */
    @EventHandler
    public void onBlockExplode (BlockExplodeEvent event) {
        removeDepots(event.blockList());
    }

    /**
     * This is technically called whenever an entity explodes rather than a block but TNT is considered an entity, so it
     * belongs here.
     *
     * @param event The EntityExplosionEvent to consider.
     */
    @EventHandler
    public void onEntityExplode (EntityExplodeEvent event) {
        removeDepots(event.blockList());
    }

    /**
     * This is a utility method to remove Depots, and the blocks they reside on, from explosion lists.
     *
     * @param blocks The block list that needs to be mutated to exclude depots and the blocks they reside on.
     */
    public void removeDepots (List<Block> blocks) {
        blocks.removeIf((b) -> {
            if (b.getType() == Material.PLAYER_HEAD) {
                Depot depot = depotManager.getDepot(b.getLocation());
                if (depot != null) return true;
            }
            Block blc = b.getLocation().add(0, 1.0, 0).getBlock();
            if (blc.getType() == Material.PLAYER_HEAD) {
                Depot depot = depotManager.getDepot(blc.getLocation());
                return depot != null;
            }
            return false;
        });
    }
}
